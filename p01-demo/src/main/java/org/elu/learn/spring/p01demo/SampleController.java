package org.elu.learn.spring.p01demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class SampleController {
    @GetMapping(value = "hello/{name}")
    public String hello(@PathVariable final String name) {
        return String.format("Hello, %s", name);
    }
}
