package org.elu.learn.spring.p01demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class P01DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(P01DemoApplication.class, args);
    }

}
